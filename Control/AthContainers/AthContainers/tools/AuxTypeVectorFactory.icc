/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
/**
 * @file AthContainers/tools/AuxTypeVectorFactory.icc
 * @author scott snyder <snyder@bnl.gov>
 * @date May, 2014
 * @brief Factory objects that creates vectors using @c AuxTypeVector.
 */


#ifndef XAOD_STANDALONE
#include "AthLinks/ElementLink.h"
#endif


namespace SG {


/**
 * @brief Apply output thinning to one item.
 *
 * Generic version; doesn't do anything.
 */
template <class T>
inline
void applyAuxThinning (T*)
{
}


#ifndef XAOD_STANDALONE
/**
 * @brief Apply output thinning to an ElementLink.
 * @param p Object being written.
 */
template <class T>
inline
void applyAuxThinning (ElementLink<T>* p)
{
  p->thin();
}


/**
 * @brief Apply output thinning to a vector of ElementLinks.
 * @param p Object being written.
 */
template <class T>
void applyAuxThinning (std::vector<ElementLink<T> >* p)
{
  for (ElementLink<T>& el : *p) {
    el.thin();
  }
}
#endif


/**
 * @brief Create a vector object of this type.
 * @param auxid ID for the variable being created.
 * @param size Initial size of the new vector.
 * @param capacity Initial capacity of the new vector.
 */
template <class T, class ALLOC>
std::unique_ptr<IAuxTypeVector> 
AuxTypeVectorFactoryImpl<T, ALLOC>::create (SG::auxid_t auxid,
                                            size_t size, size_t capacity) const
{
  return std::make_unique<AuxTypeVector<T, ALLOC> > (auxid, size, capacity);
}


/**
 * @brief Create a vector object of this type from a data blob.
 * @param auxid ID for the variable being created.
 * @param data The vector object.
 * @param isPacked If true, @c data is a @c PackedContainer.
 * @param ownFlag If true, the newly-created IAuxTypeVector object
 *                will take ownership of @c data.
 *
 * This is for types which are packable.
 */
template <class T, class ALLOC>
std::unique_ptr<IAuxTypeVector>
AuxTypeVectorFactoryImpl<T, ALLOC>::createFromData (auxid_t auxid,
                                                    void* data,
                                                    bool isPacked,
                                                    bool ownFlag,
                                                    std::true_type) const
{
  if (isPacked) {
    using unpacked_vector_type = typename SG::AuxDataTraits<T, ALLOC>::vector_type;
    using element_type = typename unpacked_vector_type::value_type;
    using unpacked_vector_allocator_type = typename SG::AuxDataTraits<T, ALLOC>::allocator_type;
    using vector_type = SG::PackedContainer<element_type, unpacked_vector_allocator_type>;
    return std::make_unique<AuxTypeVectorHolder<T, vector_type > >
      (auxid, reinterpret_cast<vector_type*>(data), ownFlag);
  }
  else {
    using vector_type = typename SG::AuxDataTraits<T, ALLOC>::vector_type;
    return std::make_unique<AuxTypeVectorHolder<T, vector_type> > (auxid, reinterpret_cast<vector_type*>(data), true);
  }
}


/**
 * @brief Create a vector object of this type from a data blob.
 * @param auxid ID for the variable being created.
 * @param data The vector object.
 * @param isPacked If true, @c data is a @c PackedContainer.
 * @param ownFlag If true, the newly-created IAuxTypeVector object
 *                will take ownership of @c data.
 *
 * This is for types which are not packable.
 */
template <class T, class ALLOC>
std::unique_ptr<IAuxTypeVector>
AuxTypeVectorFactoryImpl<T, ALLOC>::createFromData (auxid_t auxid,
                                                    void* data,
                                                    bool isPacked,
                                                    bool ownFlag,
                                                    std::false_type) const
{
  if (isPacked) std::abort();
  using vector_type = typename SG::AuxDataTraits<T, ALLOC>::vector_type;
  return std::make_unique<AuxTypeVectorHolder<T, vector_type> >
    (auxid, reinterpret_cast<vector_type*>(data), ownFlag);
}


/**
 * @brief Create a vector object of this type from a data blob.
 * @param auxid ID for the variable being created.
 * @param data The vector object.
 * @param isPacked If true, @c data is a @c PackedContainer.
 * @param ownFlag If true, the newly-created IAuxTypeVector object
 *                will take ownership of @c data.
 *
 * If the element type is T, then @c data should be a pointer
 * to a std::vector<T> object, which was obtained with @c new.
 * But if @c isPacked is @c true, then @c data
 * should instead point at an object of type @c SG::PackedContainer<T>.
 *
 * Returns a newly-allocated object.
 */
template <class T, class ALLOC>
std::unique_ptr<IAuxTypeVector>
AuxTypeVectorFactoryImpl<T, ALLOC>::createFromData (SG::auxid_t auxid,
                                                    void* data,
                                                    bool isPacked,
                                                    bool ownFlag) const
{
  return createFromData (auxid,
                         data, isPacked, ownFlag,
                         typename DataModel_detail::can_pack<T>::type());
}


/// Helper for copy; returns a pointer to the first destination object,
/// or nullptr if the destination was cleared rather than copied.
template <class T, class ALLOC>
auto
AuxTypeVectorFactoryImpl<T, ALLOC>::copyImpl (SG::auxid_t auxid,
                                              AuxVectorData& dst,
                                              size_t dst_index,
                                              const AuxVectorData& src,
                                              size_t src_index,
                                              size_t n) const -> vector_value_type*
{
  if (n == 0) return nullptr;
  auto dstptr = reinterpret_cast<vector_value_type*>(dst.getDataArray (auxid));
  if (&src == &dst) {
    // Source and destination containers are the same.
    // We don't need to bother with fetching the src pointer, but we do
    // need to check for overlaps.
    if (dst_index >= src_index && dst_index < src_index+n) {
      std::copy_backward (dstptr+src_index, dstptr+src_index+n, dstptr+dst_index+n);
    }
    else {
      std::copy (dstptr+src_index, dstptr+src_index+n, dstptr+dst_index);
    }
    return dstptr + dst_index;
  }
  else {
    auto srcptr = reinterpret_cast<const vector_value_type*>(src.getDataArrayAllowMissing (auxid));
    if (srcptr) {
      std::copy (srcptr+src_index, srcptr+src_index+n, dstptr+dst_index);
      return dstptr + dst_index;
    }
    else {
      std::fill (dstptr+dst_index, dstptr+dst_index+n, SG::Zero<vector_value_type>::zero());
      return nullptr;
    }
  }
}



/**
 * @brief Copy elements between vectors.
 * @param auxid The aux data item being operated on.
 * @param dst Container for the destination vector.
 * @param dst_index Index of the first destination element in the vector.
 * @param src Container for the source vector.
 * @param src_index Index of the first source element in the vector.
 * @param n Number of elements to copy.
 *
 * @c dst and @ src can be either the same or different.
 */
template <class T, class ALLOC>
void AuxTypeVectorFactoryImpl<T, ALLOC>::copy (SG::auxid_t auxid,
                                               AuxVectorData& dst,
                                               size_t dst_index,
                                               const AuxVectorData& src,
                                               size_t src_index,
                                               size_t n) const
{
  (void)copyImpl (auxid, dst, dst_index, src, src_index, n);
}



/**
 * @brief Copy elements between vectors, possibly applying thinning.
 * @param auxid The aux data item being operated on.
 * @param dst Container for the destination vector.
 * @param dst_index Index of the first destination element in the vector.
 * @param src Container for the source vector.
 * @param src_index Index of source element in the vector.
 * @param src_index Index of the first source element in the vector.
 * @param n Number of elements to copy.
 *
 * @c dst and @ src can be either the same or different.
 */
template <class T, class ALLOC>
void AuxTypeVectorFactoryImpl<T, ALLOC>::copyForOutput (SG::auxid_t auxid,
                                                        AuxVectorData& dst,        size_t dst_index,
                                                        const AuxVectorData& src,  size_t src_index,
                                                        size_t n) const
{
  vector_value_type* dstptr = copyImpl (auxid, dst, dst_index, src, src_index, n);
  for (size_t i = 0; i < n; i++) {
    applyAuxThinning (dstptr + i);
  }
}


/**
 * @brief Swap elements between vectors.
 * @param auxid The aux data item being operated on.
 * @param a Container for the first vector.
 * @param aindex Index of the first element in the first vector.
 * @param b Container for the second vector.
 * @param bindex Index of the first element in the second vector.
 * @param n Number of elements to swap.
 *
 * @c a and @ b can be either the same or different.
 * However, the ranges should not overlap.
 */
template <class T, class ALLOC>
void AuxTypeVectorFactoryImpl<T, ALLOC>::swap (SG::auxid_t auxid,
                                               AuxVectorData& a, size_t aindex,
                                               AuxVectorData& b, size_t bindex,
                                               size_t n) const
{
  if (n == 0) return;
  auto aptr = reinterpret_cast<vector_value_type*>(a.getDataArray (auxid));
  auto bptr = &a == &b ? aptr : reinterpret_cast<vector_value_type*>(b.getDataArray (auxid));
  for (size_t i = 0; i < n; i++) {
    std::swap (aptr[aindex+i], bptr[bindex+i]);
  }
}


/**
 * @brief Clear a range of elements within a vector.
 * @param auxid The aux data item being operated on.
 * @param dst Container holding the element
 * @param dst_index Index of the first element in the vector.
 * @param n Number of elements to clear.
 */
template <class T, class ALLOC>
void AuxTypeVectorFactoryImpl<T, ALLOC>::clear (SG::auxid_t auxid,
                                                AuxVectorData& dst,
                                                size_t dst_index,
                                                size_t n) const
{
  if (n == 0) return;
  auto ptr = reinterpret_cast<vector_value_type*>(dst.getDataArray (auxid));
  std::fill (ptr+dst_index, ptr+dst_index+n, SG::Zero<vector_value_type>::zero());
}


/**
 * @brief Return the size of an element of this vector type.
 */
template <class T, class ALLOC>
size_t AuxTypeVectorFactoryImpl<T, ALLOC>::getEltSize() const
{
  return sizeof (typename AuxTypeVector<T, ALLOC>::vector_type::value_type);
}


/**
 * @brief Return the @c type_info of the vector.
 */
template <class T, class ALLOC>
const std::type_info* AuxTypeVectorFactoryImpl<T, ALLOC>::tiVec() const
{
  return &typeid (typename AuxTypeVector<T, ALLOC>::vector_type);
}


/**
 * @brief True if the vectors created by this factory work by dynamic
 *        emulation (via @c TVirtualCollectionProxy or similar); false
 *        if the std::vector code is used directly.
 */
template <class T, class ALLOC>
bool AuxTypeVectorFactoryImpl<T, ALLOC>::isDynamic() const 
{
  return false;
}


/**
 * @brief Return the @c type_info of the vector allocator.
 */
template <class T, class ALLOC>
const std::type_info* AuxTypeVectorFactoryImpl<T, ALLOC>::tiAlloc() const 
{
  return &typeid(ALLOC);
}


/**
 * @brief Return the @c type_info of the vector allocator.
 */
template <class T, class ALLOC>
std::string AuxTypeVectorFactoryImpl<T, ALLOC>::tiAllocName() const 
{
  return SG::normalizedTypeinfoName (typeid(ALLOC));
}


} // namespace SG
