
#include "TTree.h"
#include "TFile.h"
#include "TH1F.h"
#include <iostream>

using namespace std;

void residualsFPGATrackSimHits()
{

  TFile *fLUT = TFile::Open("/afs/cern.ch/user/n/nharriso/FPGATrackSim/athena/Trigger/EFTracking/FPGATrackSim/FPGATrackSimConfTools/test/FPGATrackWorkflow/LUTs.root","","", ROOT::RCompressionSetting::EDefaults::kUseCompiledDefault,0);

  TFile *f_FPGATrackHits = TFile::Open("FPGATrackSimHits.root");

  TTree *T = (TTree*)f_FPGATrackHits->Get("T");


  Int_t barrel_ec, layer, etamod, phimod, side, phiIndex;
  std::string hashID;
  Float_t xPos, yPos, zPos, phiAxisX, phiAxisY, phiAxisZ;

  T->SetBranchAddress("barrel_ec",&barrel_ec);
  T->SetBranchAddress("layer",&layer);
  T->SetBranchAddress("phimod",&phimod);
  T->SetBranchAddress("etamod",&etamod);
  T->SetBranchAddress("hashID",&hashID);
  T->SetBranchAddress("side",&side);
  T->SetBranchAddress("phiIndex",&phiIndex);
  T->SetBranchAddress("xPos",&xPos);
  T->SetBranchAddress("yPos",&yPos);
  T->SetBranchAddress("zPos",&zPos);
  T->SetBranchAddress("phiAxisX",&phiAxisX);
  T->SetBranchAddress("phiAxisY",&phiAxisY);
  T->SetBranchAddress("phiAxisZ",&phiAxisZ);

  
  fLUT->cd();
  TTree *rLUT_tree = (TTree*)fLUT->Get("rLUT_tree");
  TTree *phiLUT_tree = (TTree*)fLUT->Get("phiLUT_tree");
  TTree *zLUT_tree = (TTree*)fLUT->Get("zLUT_tree");
  TTree *phiAxisXLUT_tree = (TTree*)fLUT->Get("phiAxisXLUT_tree");
  TTree *phiAxisYLUT_tree = (TTree*)fLUT->Get("phiAxisYLUT_tree");
  TTree *phiAxisZLUT_tree = (TTree*)fLUT->Get("phiAxisZLUT_tree");

  Float_t xPos0, yPos0, zPos0, xPos_, yPos_, zPos_, xLUT_, yLUT_, zLUT_, x_resid, y_resid, z_resid;
  Int_t layer_, etamod_, phimod_, barrel_ec_, entry, phiIndex_;
  

  TFile *fout = new TFile("residuals_strips_barrel.root","RECREATE");
  TTree *residuals = new TTree("residuals","residuals");
  residuals->Branch("entry",&entry,"entry/I");
  residuals->Branch("xPos0",&xPos0,"xPos0/F");
  residuals->Branch("yPos0",&yPos0,"yPos0/F");
  residuals->Branch("zPos0",&zPos0,"zPos0/F");
  residuals->Branch("xPos_",&xPos_,"xPos_/F");
  residuals->Branch("yPos_",&yPos_,"yPos_/F");
  residuals->Branch("zPos_",&zPos_,"zPos_/F");
  residuals->Branch("xLUT_",&xLUT_,"xLUT_/F");
  residuals->Branch("yLUT_",&yLUT_,"yLUT_/F");
  residuals->Branch("zLUT_",&zLUT_,"zLUT_/F");
  residuals->Branch("x_resid",&x_resid,"x_resid/F");
  residuals->Branch("y_resid",&y_resid,"y_resid/F");
  residuals->Branch("z_resid",&z_resid,"z_resid/F");
  residuals->Branch("layer_",&layer_,"layer/I");
  residuals->Branch("etamod_",&etamod_,"etamod/I");
  residuals->Branch("phimod_",&phimod_,"phimod/I");
  residuals->Branch("barrel_ec_",&barrel_ec_,"barrel_ec_/I");
  residuals->Branch("phiIndex_",&phiIndex_,"phiIndex_/I");
  
  
  for (int j = 0; j < T->GetEntries(); j++)
    {
      
      entry = j;
      T->GetEntry(j);

      
      std::cout << "=========================================================================" << std::endl;
      
      
      std::cout << "side = " << side << "\tlayer = " << layer << "\tphimod = " << phimod << "\tetamod = " << etamod << std::endl;
      std::cout << "xPos = " << xPos << "\tyPos = " << yPos << "\tzPos = " << zPos << std::endl;
      
      if (side == 1)
	continue;

     
      
      // rLUT has 12 entires: [0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 3, 3]
      // layer 0 has indicies 0 through 3
      // etamod:
      // r_0: index 0 = -53, ... , -1, ..., 4 ..., 8, ... 56
      // r_1: index 1 = -54, ... , -2, ..., 3 ..., 7, ... 55
      // r_2: index 2 = -55, ... , -3, ..., 2 ..., 6, ... 54
      // r_3: index 3 = -56, ... , -4, ..., 1 ..., 5, ... 53

      std::string rcut = "";
      std::string phicut = "";
      std::string zcut = "";
      
      std::string phiAxisXcut = "";
      std::string phiAxisYcut = "";
      std::string phiAxisZcut = "";
      
      if (layer == 0)
	{

	  if (etamod == 0)
	    continue;
	  
	  if ((etamod<0&&(etamod%4==-1))||(etamod>0&&(etamod%4==0)))
	    rcut = "rIndex==" + std::to_string(0);
	  else if ((etamod<0&&(etamod%4==-2))||(etamod>0&&(etamod%4==3)))
	    rcut = "rIndex==" + std::to_string(1);
	  else if ((etamod<0&&(etamod%4==-3))||(etamod>0&&(etamod%4==2)))
	    rcut = "rIndex==" + std::to_string(2);
	  else
	    rcut = "rIndex==" + std::to_string(3);
	  
	  if (etamod < 0)
	    zcut = "zIndex==" + std::to_string(etamod+56);
	  else
	    zcut = "zIndex==" + std::to_string(etamod+55);
	  
	  if ((etamod<0&&(etamod%4==-1))||(etamod>0&&(etamod%4==0)))
	    phicut = "phiIndexLUT==" + std::to_string(phimod);
	  else if ((etamod<0&&(etamod%4==-2))||(etamod>0&&(etamod%4==3)))
	    phicut = "phiIndexLUT==" + std::to_string(phimod+28);
	  else if ((etamod<0&&(etamod%4==-3))||(etamod>0&&(etamod%4==2)))
	    phicut = "phiIndexLUT==" + std::to_string(phimod+56);
	  else
	    phicut = "phiIndexLUT==" + std::to_string(phimod+84);
	  
	  phiAxisXcut = "phiAxisXIndex==" + std::to_string(phimod);
	  phiAxisYcut =	"phiAxisYIndex==" + std::to_string(phimod);
	  phiAxisZcut =	"phiAxisZIndex==" + std::to_string(phimod);
	}
      else if (layer == 1)
	{
	  if (etamod == 0)
            continue;
	  
          if ((etamod<0&&(etamod%4==-1))||(etamod>0&&(etamod%4==0)))
            rcut = "rIndex==" + std::to_string(0+4);
          else if ((etamod<0&&(etamod%4==-2))||(etamod>0&&(etamod%4==3)))
            rcut = "rIndex==" + std::to_string(1+4);
          else if ((etamod<0&&(etamod%4==-3))||(etamod>0&&(etamod%4==2)))
            rcut = "rIndex==" + std::to_string(2+4);
          else
            rcut = "rIndex==" + std::to_string(3+4);

          if (etamod < 0)
            zcut = "zIndex==" + std::to_string(etamod+56+112);
          else
            zcut = "zIndex==" + std::to_string(etamod+55+112);

          if ((etamod<0&&(etamod%4==-1))||(etamod>0&&(etamod%4==0)))
            phicut = "phiIndexLUT==" + std::to_string(phimod+112);
          else if ((etamod<0&&(etamod%4==-2))||(etamod>0&&(etamod%4==3)))
            phicut = "phiIndexLUT==" + std::to_string(phimod+40+112);
          else if ((etamod<0&&(etamod%4==-3))||(etamod>0&&(etamod%4==2)))
            phicut = "phiIndexLUT==" + std::to_string(phimod+80+112);
          else
            phicut = "phiIndexLUT==" + std::to_string(phimod+120+112);


	  std::cout << "rcut = " << rcut << std::endl;
	  std::cout << "phicut = " << phicut << std::endl;
	  std::cout << "zcut = " << zcut << std::endl;
	  
	  phiAxisXcut = "phiAxisXIndex==" + std::to_string(phimod+28);
          phiAxisYcut = "phiAxisYIndex==" + std::to_string(phimod+28);
          phiAxisZcut = "phiAxisZIndex==" + std::to_string(phimod+28);
	  
	  
	}
      else if (layer == 2)
        {
          if (etamod == 0)
            continue;

          if (((etamod<0&&etamod%2==-1)||(etamod>0&&etamod%2==0)))
            rcut = "rIndex==" + std::to_string(0+8);
          else
            rcut = "rIndex==" + std::to_string(1+8);
	  
	  if (((etamod<0&&etamod%2==-1)||(etamod>0&&etamod%2==0)))
            phicut = "phiIndexLUT==" + std::to_string(phimod+112+160);
          else 
            phicut = "phiIndexLUT==" + std::to_string(phimod+56+112+160);

          if (etamod < 0)
            zcut = "zIndex==" + std::to_string(etamod+28+2*112);
          else
            zcut = "zIndex==" + std::to_string(etamod+27+2*112);

	  
          std::cout << "rcut = " << rcut << std::endl;
          std::cout << "phicut = " << phicut << std::endl;
          std::cout << "zcut = " << zcut << std::endl;
	  
          phiAxisXcut = "phiAxisXIndex==" + std::to_string(phimod+28+40);
          phiAxisYcut = "phiAxisYIndex==" + std::to_string(phimod+28+40);
          phiAxisZcut = "phiAxisZIndex==" + std::to_string(phimod+28+40);

	  
        }
      else 
        {
          if (etamod == 0)
            continue;

	  if (((etamod<0&&etamod%2==-1)||(etamod>0&&etamod%2==0)))
            rcut = "rIndex==" + std::to_string(0+10);
          else
            rcut = "rIndex==" + std::to_string(1+10);

          if (etamod < 0)
            zcut = "zIndex==" + std::to_string(etamod+28+112+112+56);
          else
            zcut = "zIndex==" + std::to_string(etamod+27+112+112+56);

          if (((etamod<0&&etamod%2==-1)||(etamod>0&&etamod%2==0)))
            phicut = "phiIndexLUT==" + std::to_string(phimod+112+160+112);
          else
            phicut = "phiIndexLUT==" + std::to_string(phimod+72+112+160+112);

	  
          std::cout << "rcut = " << rcut << std::endl;
          std::cout << "phicut = " << phicut << std::endl;
          std::cout << "zcut = " << zcut << std::endl;
	  
	  
	  phiAxisXcut = "phiAxisXIndex==" + std::to_string(phimod+28+40+56);
          phiAxisYcut = "phiAxisYIndex==" + std::to_string(phimod+28+40+56);
          phiAxisZcut = "phiAxisZIndex==" + std::to_string(phimod+28+40+56);




        }


      TH1F *r_th1f = new TH1F("r_th1f","r_th1f",1000,0,1000);
      TH1F *phi_th1f = new TH1F("phi_th1f","phi_th1f",1000,-3.2,3.2);
      TH1F *z_th1f = new TH1F("z_th1f","z_th1f",1000,-3000,3000);
      TH1F *phiAxisX_th1f = new TH1F("phiAxisX_th1f","phiAxisX_th1f",1000,-1,1);
      TH1F *phiAxisY_th1f =	new TH1F("phiAxisY_th1f","phiAxisY_th1f",1000,-1,1);
      TH1F *phiAxisZ_th1f =	new TH1F("phiAxisZ_th1f","phiAxisZ_th1f",1000,-1,1);
      //fLUT->cd();
      rLUT_tree->Draw("radius>>r_th1f",rcut.c_str());
      phiLUT_tree->Draw("phi>>phi_th1f",phicut.c_str());
      zLUT_tree->Draw("zcoord>>z_th1f",zcut.c_str());
      phiAxisXLUT_tree->Draw("phiAxisXvalue>>phiAxisX_th1f",phiAxisXcut.c_str());
      phiAxisYLUT_tree->Draw("phiAxisYvalue>>phiAxisY_th1f",phiAxisYcut.c_str());
      phiAxisZLUT_tree->Draw("phiAxisZvalue>>phiAxisZ_th1f",phiAxisZcut.c_str());
      
      std::string cut = "barrel_ec==0&&layer==" + std::to_string(layer) + "&&phimod==" + std::to_string(phimod) + "&&etamod==" + std::to_string(etamod);
      
      xPos_ = xPos;
      yPos_ = yPos;
      zPos_ = zPos;
      
      float r = r_th1f->GetMean();
      float phi = phi_th1f->GetMean();
      float z = z_th1f->GetMean();
      
      float xLUT = r*cos(phi);
      float yLUT = r*sin(phi);
      float zLUT = z;
      
      
      std::cout << "rLUT = " << r << std::endl;
      std::cout << "phiLUT = " << phi << std::endl;
      std::cout << "xLUT = " << xLUT << std::endl;
      std::cout << "yLUT = " << yLUT << std::endl;
      std::cout << "zLUT = " << zLUT << std::endl;
      
      
      float phiAxisXLUT = phiAxisX_th1f->GetMean();
      float	phiAxisYLUT = phiAxisY_th1f->GetMean();
      float	phiAxisZLUT = phiAxisZ_th1f->GetMean();
      
     
      std::cout << "phiAxisXLUT = " << phiAxisXLUT << std::endl;
      std::cout << "phiAxisYLUT = "	<< phiAxisYLUT << std::endl;
      std::cout << "phiAxisZLUT = "	<< phiAxisZLUT << std::endl;
      
      xPos0 = xLUT;
      yPos0 = yLUT;
      zPos0 = zLUT;
      
      xLUT_ = xLUT + ((float) phiIndex)/1280.*96.64*phiAxisXLUT;
      yLUT_ = yLUT + ((float) phiIndex)/1280.*96.64*phiAxisYLUT;
      zLUT_ = zLUT + ((float) phiIndex)/1280.*96.64*phiAxisZLUT;

      phiIndex_ = phiIndex;
      
      std::cout << "xLUT_ = " << xLUT_ << std::endl;
      std::cout << "yLUT_ = " << yLUT_ << std::endl;
      std::cout << "zLUT_ = " << zLUT_ << std::endl;
      
      
      layer_ = layer;
      phimod_ = phimod;
      etamod_ = etamod;
      
      barrel_ec_ = 0;
      
      x_resid = xLUT_ - xPos;
      y_resid = yLUT_ - yPos;
      z_resid = zLUT_ - zPos;
      fout->cd();
      residuals->Fill();
      
      
      
    }
  fout->Write();
  
}
