/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/


#include "FPGATrackSimObjects/FPGATrackSimEventInputHeader.h"
#include "FPGATrackSimObjects/FPGATrackSimEventInfo.h"
#include "FPGATrackSimObjects/FPGATrackSimOfflineTrack.h"
#include "FPGATrackSimObjects/FPGATrackSimOfflineHit.h"
#include "FPGATrackSimObjects/FPGATrackSimTruthTrack.h"
#include "FPGATrackSimObjects/FPGATrackSimHit.h"

#include "SCT_ReadoutGeometry/StripStereoAnnulusDesign.h"

#include "TrkDetElementBase/TrkDetElementBase.h"

#include "ReadoutGeometryBase/DetectorDesign.h"
#include "ReadoutGeometryBase/SiIntersect.h"
#include "ReadoutGeometryBase/SiCommonItems.h"
#include "ReadoutGeometryBase/SiCellId.h"
#include "ReadoutGeometryBase/SiReadoutCellId.h"
#include "GeoPrimitives/CLHEPtoEigenConverter.h"


#include "StoreGate/DataHandle.h"

#include "IdDictDetDescr/IdDictManager.h"
#include "InDetPrepRawData/SiClusterContainer.h"
#include "InDetPrepRawData/SiClusterCollection.h"
#include "InDetRawData/InDetRawDataCollection.h"
#include "InDetRawData/InDetRawDataContainer.h"
#include "GeoModelKernel/GeoVDetectorElement.h"
#include "InDetRawData/InDetRawDataCLASS_DEF.h"
#include "InDetSimData/SCT_SimHelper.h"
#include "InDetSimData/PixelSimHelper.h"
#include "InDetReadoutGeometry/SiDetectorDesign.h"

#include "ReadoutGeometryBase/SiCellId.h"
#include "ReadoutGeometryBase/SiReadoutCellId.h"

#include "TrkParameters/TrackParameters.h"

#include "AtlasHepMC/GenEvent.h"
#include "AtlasHepMC/GenVertex.h"
#include "AtlasHepMC/GenParticle.h"
#include "TruthUtils/HepMCHelpers.h"

#include "InDetRIO_OnTrack/SiClusterOnTrack.h"


#include "ReadoutGeometryBase/SolidStateDetectorElementBase.h"

#include "TrkSurfaces/PlaneSurface.h"


#include "GaudiKernel/IPartPropSvc.h"
#include "FPGATrackSimSGToRawHitsTool.h"

#include "TFile.h"
#include "TH1F.h"
#include "TTree.h"

#include <bits/stdc++.h>
using namespace std;

#include <iostream>
#include <fstream>

using Trk::distPhi;
using Trk::distEta;
using Trk::distDepth;



namespace {
  // A few constants for truth cuts
  const float FPGATrackSim_PT_TRUTHMIN = 400.;
  const float FPGATrackSim_Z_TRUTHMIN = 2300.;
}

FPGATrackSimSGToRawHitsTool::FPGATrackSimSGToRawHitsTool(const std::string& algname, const std::string& name, const IInterface* ifc) :
  base_class(algname, name, ifc)
{}

StatusCode FPGATrackSimSGToRawHitsTool::initialize() {

  ATH_MSG_DEBUG("FPGATrackSimSGToRawHitsTool::initialize()");

  if(!m_truthToTrack.empty() ) ATH_CHECK(m_truthToTrack.retrieve());  
  if(!m_extrapolator.empty()) ATH_CHECK(m_extrapolator.retrieve());
  ATH_CHECK(m_beamSpotKey.initialize());


  IPartPropSvc* partPropSvc = nullptr;
  ATH_CHECK(service("PartPropSvc", partPropSvc));
  m_particleDataTable = partPropSvc->PDT();

  ATH_CHECK(detStore()->retrieve(m_PIX_mgr, "ITkPixel"));
  ATH_CHECK(detStore()->retrieve(m_pixelId, "PixelID"));
  ATH_CHECK(detStore()->retrieve(m_SCT_mgr, "ITkStrip"));
  ATH_CHECK(detStore()->retrieve(m_sctId, "SCT_ID"));

  ATH_CHECK(m_eventInfoKey.initialize());
  ATH_CHECK(m_pixelClusterContainerKey.initialize(SG::AllowEmpty));
  ATH_CHECK(m_sctClusterContainerKey.initialize(SG::AllowEmpty));

  ATH_CHECK(m_offlineTracksKey.initialize(SG::AllowEmpty));

  ATH_CHECK(m_mcCollectionKey.initialize(SG::AllowEmpty));
  ATH_CHECK(m_pixelSDOKey.initialize(SG::AllowEmpty));
  ATH_CHECK(m_stripSDOKey.initialize(SG::AllowEmpty));
  ATH_CHECK(m_pixelRDOKey.initialize());
  ATH_CHECK(m_stripRDOKey.initialize());

  ATH_MSG_DEBUG("Initialization complete");
  return StatusCode::SUCCESS;
}


StatusCode FPGATrackSimSGToRawHitsTool::finalize() {
  return StatusCode::SUCCESS;
}


/** This function get from the SG the inner detector raw hits
  and prepares them for FPGATrackSim simulation */
StatusCode FPGATrackSimSGToRawHitsTool::readData(FPGATrackSimEventInputHeader* header, const EventContext& eventContext)
{
  
  m_eventHeader = header; //take the external pointer
  auto eventInfo = SG::makeHandle(m_eventInfoKey, eventContext);
  //Filled to variable / start event
  FPGATrackSimEventInfo event_info;
  event_info.setRunNumber(eventInfo->runNumber());
  event_info.setEventNumber(eventInfo->eventNumber());
  event_info.setLB(eventInfo->lumiBlock());
  event_info.setBCID(eventInfo->bcid());
  event_info.setaverageInteractionsPerCrossing(eventInfo->averageInteractionsPerCrossing());
  event_info.setactualInteractionsPerCrossing(eventInfo->actualInteractionsPerCrossing());
  event_info.setextendedLevel1ID(eventInfo->extendedLevel1ID());
  event_info.setlevel1TriggerType(eventInfo->level1TriggerType());
  //  event_info.setlevel1TriggerInfo(eventInfo->level1TriggerInfo ()); // unclear if needed, TODO come back to it
  m_eventHeader->newEvent(event_info);//this also reset all variables
  HitIndexMap hitIndexMap; // keep running index event-unique to each hit
  HitIndexMap pixelClusterIndexMap;
  // get pixel and sct cluster containers
  // dump raw silicon data
  ATH_MSG_DEBUG("Dump raw silicon data");
  ATH_CHECK(readRawSilicon(hitIndexMap,  eventContext));
  FPGATrackSimOptionalEventInfo optional;
  if (m_readOfflineClusters) {
    std::vector <FPGATrackSimCluster> clusters;
    ATH_CHECK(readOfflineClusters(clusters, eventContext));
    for (const auto& cluster : clusters) optional.addOfflineCluster(cluster);
    ATH_MSG_DEBUG("Saved " << optional.nOfflineClusters() << " offline clusters");
    ATH_CHECK(dumpPixelClusters(pixelClusterIndexMap, eventContext));
  }
  if (m_readTruthTracks) {
    std::vector <FPGATrackSimTruthTrack> truth;
    ATH_CHECK(readTruthTracks(truth, eventContext));
    for (const FPGATrackSimTruthTrack& trk : truth) optional.addTruthTrack(trk);
    ATH_MSG_DEBUG("Saved " << optional.nTruthTracks() << " truth tracks");
  }
  std::vector <FPGATrackSimOfflineTrack> offline;
  if (m_readOfflineTracks) {
    ATH_CHECK(readOfflineTracks(offline, eventContext));
    for (const FPGATrackSimOfflineTrack& trk : offline) optional.addOfflineTrack(trk);
    ATH_MSG_DEBUG("Saved " << optional.nOfflineTracks() << " offline tracks");
  }
  m_eventHeader->setOptional(optional);
  ATH_MSG_DEBUG(*m_eventHeader);
  ATH_MSG_DEBUG("End of execute()");
  return StatusCode::SUCCESS;
}


StatusCode FPGATrackSimSGToRawHitsTool::readOfflineTracks(std::vector<FPGATrackSimOfflineTrack>& offline, const EventContext& eventContext)
{
  auto offlineTracksHandle = SG::makeHandle(m_offlineTracksKey, eventContext);
  ATH_MSG_DEBUG("read Offline tracks, size= " << offlineTracksHandle->size());

  int iTrk = -1;
  for (const xAOD::TrackParticle* trackParticle : *offlineTracksHandle) {
    iTrk++;
    FPGATrackSimOfflineTrack tmpOfflineTrack;
    tmpOfflineTrack.setQOverPt(trackParticle->pt() > 0 ? trackParticle->charge() / trackParticle->pt() : 0);
    tmpOfflineTrack.setEta(trackParticle->eta());
    tmpOfflineTrack.setPhi(trackParticle->phi());

    const Trk::TrackStates* trackStates = trackParticle->track()->trackStateOnSurfaces();
    if (trackStates == nullptr) {
      ATH_MSG_ERROR("missing trackStatesOnSurface");
      return StatusCode::FAILURE;
    }
    for (const Trk::TrackStateOnSurface* tsos : *trackStates) {
      if (tsos == nullptr) continue;
      if (tsos->type(Trk::TrackStateOnSurface::Measurement)) {
        const Trk::MeasurementBase* measurement = tsos->measurementOnTrack();
        if (tsos->trackParameters() != nullptr &&
          tsos->trackParameters()->associatedSurface().associatedDetectorElement() != nullptr &&
          tsos->trackParameters()->associatedSurface().associatedDetectorElement()->identify() != 0
          ) {
          const Trk::RIO_OnTrack* hit = dynamic_cast <const Trk::RIO_OnTrack*>(measurement);
          const Identifier& hitId = hit->identify();
          FPGATrackSimOfflineHit tmpOfflineHit;
          if (m_pixelId->is_pixel(hitId)) {
            tmpOfflineHit.setIsPixel(true);
            tmpOfflineHit.setIsBarrel(m_pixelId->is_barrel(hitId));

            const InDetDD::SiDetectorElement* sielement = m_PIX_mgr->getDetectorElement(hitId);
            tmpOfflineHit.setClusterID(sielement->identifyHash());
            tmpOfflineHit.setTrackNumber(iTrk);
            tmpOfflineHit.setLayer(m_pixelId->layer_disk(hitId));
            tmpOfflineHit.setLocX((float)measurement->localParameters()[Trk::locX]);
            tmpOfflineHit.setLocY((float)measurement->localParameters()[Trk::locY]);
          }
          else if (m_sctId->is_sct(hitId)) {
            tmpOfflineHit.setIsPixel(false);
            tmpOfflineHit.setIsBarrel(m_sctId->is_barrel(hitId));
            const InDetDD::SiDetectorElement* sielement = m_SCT_mgr->getDetectorElement(hitId);
            tmpOfflineHit.setClusterID(sielement->identifyHash());
            tmpOfflineHit.setTrackNumber(iTrk);
            tmpOfflineHit.setLayer(m_sctId->layer_disk(hitId));
            tmpOfflineHit.setLocX(((float)measurement->localParameters()[Trk::locX]));
            tmpOfflineHit.setLocY(-99999.9);
          }
          tmpOfflineTrack.addHit(tmpOfflineHit);
        }
      }
    }
    offline.push_back(tmpOfflineTrack);
  }//end of loop over tracks


  return StatusCode::SUCCESS;
}



// dump silicon channels with geant matching information.
StatusCode
FPGATrackSimSGToRawHitsTool::readRawSilicon(HitIndexMap& hitIndexMap, const EventContext& eventContext) // const cannot make variables push back to DataInput
{
  ATH_MSG_DEBUG("read silicon hits");
  unsigned int hitIndex = 0u;

  ATH_CHECK(readPixelSimulation(hitIndexMap, hitIndex, eventContext));
  ATH_CHECK(readStripSimulation(hitIndexMap, hitIndex, eventContext));

  return StatusCode::SUCCESS;
}


StatusCode
FPGATrackSimSGToRawHitsTool::readPixelSimulation(HitIndexMap& hitIndexMap, unsigned int& hitIndex, const EventContext& eventContext) {

  auto pixelSDOHandle = SG::makeHandle(m_pixelSDOKey, eventContext);
  auto pixelRDOHandle = SG::makeHandle(m_pixelRDOKey, eventContext);

  ATH_MSG_DEBUG("Found Pixel SDO Map");

  for (const InDetRawDataCollection<PixelRDORawData>* pixel_rdoCollection : *pixelRDOHandle) {
    if (pixel_rdoCollection == nullptr) { continue; }
    // loop on all RDOs
    for (const PixelRDORawData* pixelRawData : *pixel_rdoCollection) {
      Identifier rdoId = pixelRawData->identify();
      // get the det element from the det element collection
      const InDetDD::SiDetectorElement* sielement = m_PIX_mgr->getDetectorElement(rdoId); assert(sielement);

      Amg::Vector2D LocalPos = sielement->rawLocalPositionOfCell(rdoId);
      Amg::Vector3D globalPos = sielement->globalPosition(LocalPos);
      InDetDD::SiCellId cellID = sielement->cellIdFromIdentifier(rdoId);

      // update map between pixel identifier and event-unique hit index.
      // ganged pixels (nCells==2) get two entries.
      hitIndexMap[rdoId] = hitIndex;
      const int nCells = sielement->numberOfConnectedCells(cellID);
      if (nCells == 2) {
        const InDetDD::SiCellId tmpCell = sielement->connectedCell(cellID, 1);
        const Identifier tmpId = sielement->identifierFromCellId(tmpCell);
        hitIndexMap[tmpId] = hitIndex; // add second entry for ganged pixel ID
      }
      // if there is simulation truth available, try to retrieve the "most likely" barcode for this pixel.
      HepMC::ConstGenParticlePtr bestParent = nullptr;
      FPGATrackSimInputUtils::ParentBitmask parentMask;
      HepMcParticleLink::ExtendedBarCode bestExtcode;
      if (!m_pixelSDOKey.empty()) {
        InDetSimDataCollection::const_iterator iter(pixelSDOHandle->find(rdoId));
        if (nCells > 1 && iter == pixelSDOHandle->end()) {
          InDetDD::SiReadoutCellId SiRC(m_pixelId->phi_index(rdoId), m_pixelId->eta_index(rdoId));
          for (int ii = 0; ii < nCells && iter == pixelSDOHandle->end(); ++ii) {
            iter = pixelSDOHandle->find(sielement->identifierFromCellId(sielement->design().connectedCell(SiRC, ii)));
          }
        } // end search for correct ganged pixel
        // if SDO found for this pixel, associate the particle. otherwise leave unassociated.
        if (iter != pixelSDOHandle->end()) getTruthInformation(iter, parentMask, bestExtcode, bestParent);
      } // end if pixel truth available
      ++hitIndex;

      // push back the hit information  to DataInput for HitList
      FPGATrackSimHit tmpSGhit;
      tmpSGhit.setHitType(HitType::unmapped);
      tmpSGhit.setDetType(SiliconTech::pixel);
      tmpSGhit.setIdentifierHash(sielement->identifyHash());

      int barrel_ec = m_pixelId->barrel_ec(rdoId);
      if (barrel_ec == 0)
        tmpSGhit.setDetectorZone(DetectorZone::barrel);
      else if (barrel_ec == 2)
        tmpSGhit.setDetectorZone(DetectorZone::posEndcap);
      else if (barrel_ec == -2)
        tmpSGhit.setDetectorZone(DetectorZone::negEndcap);

      tmpSGhit.setLayerDisk(m_pixelId->layer_disk(rdoId));
      tmpSGhit.setPhiModule(m_pixelId->phi_module(rdoId));
      tmpSGhit.setEtaModule(m_pixelId->eta_module(rdoId));
      tmpSGhit.setPhiIndex(m_pixelId->phi_index(rdoId));
      tmpSGhit.setEtaIndex(m_pixelId->eta_index(rdoId));
      tmpSGhit.setEtaWidth(0);
      tmpSGhit.setPhiWidth(0);
      tmpSGhit.setX(globalPos[Amg::x]);
      tmpSGhit.setY(globalPos[Amg::y]);
      tmpSGhit.setZ(globalPos[Amg::z]);
      tmpSGhit.setToT(pixelRawData->getToT());
      index_type index, position;
      bestExtcode.eventIndex(index, position);
      if (bestParent)
        tmpSGhit.setEventIndex(index);
      else
        tmpSGhit.setEventIndex(std::numeric_limits<long>::max());
      tmpSGhit.setBarcode((long)(bestParent ? bestExtcode.uid() : std::numeric_limits<long>::max())); // FIXME
      tmpSGhit.setBarcodePt(static_cast<unsigned long>(std::ceil(bestParent ? bestParent->momentum().perp() : 0.)));
      tmpSGhit.setParentageMask(parentMask.to_ulong());

      // Add truth
      FPGATrackSimMultiTruth mt;
      FPGATrackSimMultiTruth::Barcode uniquecode(tmpSGhit.getEventIndex(), tmpSGhit.getBarcode());
      mt.maximize(uniquecode, tmpSGhit.getBarcodePt());
      tmpSGhit.setTruth(mt);

      m_eventHeader->addHit(tmpSGhit);
    } // end for each RDO in the collection
  } // for each pixel RDO collection

  return StatusCode::SUCCESS;
}

StatusCode
FPGATrackSimSGToRawHitsTool::readStripSimulation(HitIndexMap& hitIndexMap, unsigned int& hitIndex, const EventContext& eventContext) {

  // 05/30/24: had started with strips in barrel.
  // barrel:
  // layer 0 has 28 phi mod and 112 etamod (-56, ... -1, 1, .... 56)
  // layer 1 has 40 phi mod and 112 etamod  " "
  // layer 2 has 56 phi mod and 56 etamod (-28, ... -1, 1, ... 28)
  // layer 3 has 72 phi mod and 56 etamod " "
  // total = 14784 modules
  // for one event only, generate a set of "origin" FPGATrackSimHits using hashIDs of origin of silicon strip modules, stored as hashIDs in hashID.txt                                                                                            
  // print all info: hashID, etamod, phimod, layer, xPos, yPos, zPos, phiAxisX, phiAxisY, phiAxisZ (should only need phiAxisX, phiAxisY but print all if needed for studying)
  bool loop_over_origin_hash_id_barrel = false;
  if (loop_over_origin_hash_id_barrel)
    {

      std::string line;
      std::stringstream ss;
      int count = 0;


      
      std::ifstream hashIDFile("/afs/cern.ch/user/n/nharriso/hash_id.txt");
      while (hashIDFile.good())
	{
	  getline(hashIDFile,line);
	  count++;
	  if(count > 14784) // there is a better way to do this... 
	    break;
	  
	  Identifier originID;
	  originID.set(line);

	  // get identifer                                                                                                   
	  const InDetDD::SiDetectorElement *siOriginElement = m_SCT_mgr->getDetectorElement(originID);
	  std::cout << "count = " << count << std::endl;
	  
	  bool generate_fpga_track_sim_hits_all_phiIndex = true;
	  if (generate_fpga_track_sim_hits_all_phiIndex)
	    {
	      for (int phiIndex = 0; phiIndex < 1280; phiIndex++)
		{
		  Amg::Vector2D localPosition = siOriginElement->design().localPositionOfCell(InDetDD::SiCellId(phiIndex));
		  Identifier siElementPhiIndexID = siOriginElement->identifierOfPosition(localPosition);
		  const InDetDD::SiDetectorElement *siElementPhiIndex = m_SCT_mgr->getDetectorElement(siElementPhiIndexID);
		  
		  Amg::Vector2D LocalPosFPGAHit = siElementPhiIndex->rawLocalPositionOfCell(siElementPhiIndexID);
		  std::pair<Amg::Vector3D, Amg::Vector3D> endsOfStripFPGAHit = siElementPhiIndex->endsOfStrip(LocalPosFPGAHit);

		  // make FPGATrackSimHit FPGAHit at phiIndex                                                                               
		  FPGATrackSimHit FPGAHit;
		  FPGAHit.setHitType(HitType::unmapped);
		  FPGAHit.setDetType(SiliconTech::strip);
		  
		  // should trivially be in barrel since initial hashID.txt is strips in barrel                                    
		  int barrel_ec = m_sctId->barrel_ec(siElementPhiIndexID);
		  if (barrel_ec == 0)
		    FPGAHit.setDetectorZone(DetectorZone::barrel);
		  else if (barrel_ec == 2)
		    FPGAHit.setDetectorZone(DetectorZone::posEndcap);
		  else if (barrel_ec == -2)
		    FPGAHit.setDetectorZone(DetectorZone::negEndcap);

		  FPGAHit.setLayerDisk(m_sctId->layer_disk(siElementPhiIndexID));
		  FPGAHit.setPhiModule(m_sctId->phi_module(siElementPhiIndexID));
		  FPGAHit.setEtaModule(m_sctId->eta_module(siElementPhiIndexID));
		  FPGAHit.setPhiIndex(m_sctId->strip(siElementPhiIndexID));
		  FPGAHit.setEtaIndex(m_sctId->row(siElementPhiIndexID));
		  FPGAHit.setSide(m_sctId->side(siElementPhiIndexID));
		  
		  FPGAHit.setX(0.5 * (endsOfStripFPGAHit.first.x() + endsOfStripFPGAHit.second.x()));
		  FPGAHit.setY(0.5 * (endsOfStripFPGAHit.first.y() + endsOfStripFPGAHit.second.y()));
		  FPGAHit.setZ(0.5 * (endsOfStripFPGAHit.first.z() + endsOfStripFPGAHit.second.z()));
		  
		  ofstream myfile;
		  myfile.open ("FPGATrackSimHits_allPhiIndex.dat",std::ios_base::app);
		  myfile << barrel_ec << "\t" << FPGAHit.getLayerDisk() << "\t" << FPGAHit.getPhiModule() << "\t" << FPGAHit.getEtaModule() << "\t" << siElementPhiIndexID.getString() << "\t" << FPGAHit.getSide() << "\t" << FPGAHit.getPhiIndex() << "\t" << FPGAHit.getX() << "\t" << FPGAHit.getY() << "\t" << FPGAHit.getZ() << "\t" << siElementPhiIndex->phiAxis().x() << "\t" << siElementPhiIndex->phiAxis().y() << "\t" << siElementPhiIndex->phiAxis().z() << std::endl;
		  myfile.close();
		  
		}
	    }
	      

	  // Generate layout file
	  bool generate_origin_hits = false;

	  if (generate_origin_hits)
	    {
	      Amg::Vector2D LocalPosOrigin = siOriginElement->rawLocalPositionOfCell(originID);
	      std::pair<Amg::Vector3D, Amg::Vector3D> endsOfStripOrigin = siOriginElement->endsOfStrip(LocalPosOrigin);
	      
	      // make origin FPGATrackSimHit                                                                                     
	      FPGATrackSimHit originHit;
	      originHit.setHitType(HitType::unmapped);
	      originHit.setDetType(SiliconTech::strip);
	      
	      // should trivially be in barrel since initial hashID.txt is strips in barrel                                    
	      int barrel_ec = m_sctId->barrel_ec(originID);
	      if (barrel_ec == 0)
		originHit.setDetectorZone(DetectorZone::barrel);
	      else if (barrel_ec == 2)
		originHit.setDetectorZone(DetectorZone::posEndcap);
	      else if (barrel_ec == -2)
		originHit.setDetectorZone(DetectorZone::negEndcap);
	      
	      originHit.setLayerDisk(m_sctId->layer_disk(originID));
	      originHit.setPhiModule(m_sctId->phi_module(originID));
	      originHit.setEtaModule(m_sctId->eta_module(originID));
	      originHit.setPhiIndex(m_sctId->strip(originID));
	      originHit.setEtaIndex(m_sctId->row(originID));
	      originHit.setSide(m_sctId->side(originID));
	      
	      originHit.setX(0.5 * (endsOfStripOrigin.first.x() + endsOfStripOrigin.second.x()));
	      originHit.setY(0.5 * (endsOfStripOrigin.first.y() + endsOfStripOrigin.second.y()));
	      originHit.setZ(0.5 * (endsOfStripOrigin.first.z() + endsOfStripOrigin.second.z()));
	      

	      /*
	      // trying to understand how m_phiAxis is determined in FPGATrackSim
	      // how is m_phiAxisCLHEP defined?
	      // where in m_design is this specified?
	      // where in TDR is this?
	      // can we remove all LUT related to phiAxis?
	      // phiAxisZ is just sin(+/- 26 mRad)
	      
	      const GeoVFullPhysVol *GeoVDetectorElement;
	      
	      const HepGeom::Transform3D &geoTransform = GeoVDetectorElement->getMaterialGeom()->getAbsoluteTransform() * m_design->moduleShift();
	      
	      // depthAxis, phiAxis, and etaAxis are defined to be x,y,z respectively for all detectors for hit local frame.                                                               
	      static const HepGeom::Vector3D<double> localAxes[3] = {
	      HepGeom::Vector3D<double>(1,0,0),
	      HepGeom::Vector3D<double>(0,1,0),
	      HepGeom::Vector3D<double>(0,0,1)
	      };
	      
	      std::cout << "Trk::distPhi = " << Trk::distPhi << std::endl;
	      
	      static const HepGeom::Vector3D<double> & localRecoPhiAxis = localAxes[Trk::distPhi];     // Defined to be same as x axis                                                     
	      
	      m_transformCLHEP = geoTransform * recoToHitTransm();
	      m_phiAxisCLHEP = m_transformCLHEP * localRecoPhiAxis;
	      */
	      
	     
	      // make new geometry.dat file                                                                                    
	      ofstream myfile;
	      myfile.open ("ITkStrip_geometry_strips_barrel_update.dat",std::ios_base::app);
	      myfile << barrel_ec << "\t" << originHit.getLayerDisk() << "\t" << originHit.getPhiModule() << "\t" << originHit.getEtaModule() << "\t" << originID.getString() << "\t" << originHit.getX() << "\t" << originHit.getY() << "\t" << originHit.getZ() << "\t" << siOriginElement->etaAxis().x() << "\t" << siOriginElement->etaAxis().y() << "\t" << siOriginElement->etaAxis().z() << "\t" << siOriginElement->phiAxis().x() << "\t" << siOriginElement->phiAxis().y() << "\t" << siOriginElement->phiAxis().z() << std::endl;
	      myfile.close();
	    }

	}
    }

  // 06/05/24: loop over hashIDs of strip modules in endcaps
  // https://gitlab.cern.ch/atlas/athena/-/blob/main/InnerDetector/InDetDetDescr/SCT_ReadoutGeometry/SCT_ReadoutGeometry/StripStereoAnnulusDesign.h?ref_type=heads
  //    StereoAnnulus shaped sensor design.
  //    Ref. N.P. Hessey "Building a Stereo-angle into strip-sensors for the ATLAS-Upgrade Inner-Tracker Endcaps"
  //    2012.
  //
  //    Local reference system is centred on the beamline (i.e. not in the wafer centre; it is made in GeoModel as
  //    an intersection of a tube with a (translated) generic trap. x is along the centre strip; y is in phi direction;
  //    z is the depth direction. Strips are on the +ve z side.
  //
  //        /)       ^ y
  //       /  )      |
  //      )   )      --> x
  //       \  )
  //        \)       z towards you and you are looking at the strip side
  //
  //    A sensor can have many rows of strips, with varying numbers of strips in each row. Row-lengths can be different.
  //
  //    Strips are identified by a single index, starting at 0 in the inner-most row low-y side, 
  //    and ending at totalNStrips - 1 
  
  
  bool loop_over_origin_hash_id_endcaps = true;
  if (loop_over_origin_hash_id_endcaps)
    {
      std::cout << "HERE" << std::endl;
      

      std::vector<InDetDD::SiDetectorElement*> siOriginElements = {};
      std::string line;
      std::stringstream ss;
      int count = 0;


      
      std::ifstream hashIDFile("/afs/cern.ch/user/n/nharriso/hash_id_endcap.txt");
      while (hashIDFile.good())
        {
          getline(hashIDFile,line);
          count++;
          if(count > 9984) // there is a better way to do this...                                     
            break;
	  if(count >1)
	    continue;
	  
	  std::cout << "line # " << count << std::endl;
	  
          Identifier originID;
          originID.set(line);
	  
	  const InDetDD::SiDetectorElement *siOriginElement = m_SCT_mgr->getDetectorElement(originID);
	  
	  const InDetDD::StripStereoAnnulusDesign *design = dynamic_cast<const InDetDD::StripStereoAnnulusDesign *>(&siOriginElement->design());

	  Amg::Vector2D LocalPosOrigin = siOriginElement->rawLocalPositionOfCell(originID);
	  std::pair<Amg::Vector3D, Amg::Vector3D> endsOfStripOrigin = siOriginElement->endsOfStrip(LocalPosOrigin);
	  
	  // make origin FPGATrackSimHit                                                                                                                                          
	  FPGATrackSimHit originHit;
	  originHit.setHitType(HitType::unmapped);
	  originHit.setDetType(SiliconTech::strip);
	  
	  // should trivially be in barrel since initial hashID.txt is strips in barrel                                                                                           
	  int barrel_ec = m_sctId->barrel_ec(originID);
	  if (barrel_ec == 0)
	    originHit.setDetectorZone(DetectorZone::barrel);
	  else if (barrel_ec == 2)
	    originHit.setDetectorZone(DetectorZone::posEndcap);
	  else if (barrel_ec == -2)
	    originHit.setDetectorZone(DetectorZone::negEndcap);
	  
	  originHit.setLayerDisk(m_sctId->layer_disk(originID));
	  originHit.setPhiModule(m_sctId->phi_module(originID));
	  originHit.setEtaModule(m_sctId->eta_module(originID));
	  originHit.setPhiIndex(m_sctId->strip(originID));
	  originHit.setEtaIndex(m_sctId->row(originID));
	  originHit.setSide(m_sctId->side(originID));
	  
	  originHit.setX(0.5 * (endsOfStripOrigin.first.x() + endsOfStripOrigin.second.x()));
	  originHit.setY(0.5 * (endsOfStripOrigin.first.y() + endsOfStripOrigin.second.y()));
	  originHit.setZ(0.5 * (endsOfStripOrigin.first.z() + endsOfStripOrigin.second.z()));


	  bool generate_origin_hits = false;

	  if (generate_origin_hits)
	    {
	      // make new geometry.dat file                                                                                                                                                    
              ofstream myfile;
              myfile.open ("ITkStrip_geometry_strips_endcap_update.dat",std::ios_base::app);
              myfile << barrel_ec << "\t" << originHit.getLayerDisk() << "\t" << originHit.getPhiModule() << "\t" << originHit.getEtaModule() << "\t" << originID.getString() << "\t" << originHit.getR() << "\t" << originHit.getGPhi() << "\t" << originHit.getZ() << "\t" << siOriginElement->etaAxis().x() << "\t" << siOriginElement->etaAxis().y() << "\t" << siOriginElement->etaAxis().z() << "\t" << siOriginElement->phiAxis().x() << "\t" << siOriginElement->phiAxis().y() << "\t" << siOriginElement->phiAxis().z() << std::endl;
              myfile.close();

	    }
	  
	  /*
	  std::cout << "originHit.getLayerDisk() = " << originHit.getLayerDisk() << std::endl;
	  std::cout << "originHit.getPhiModule() = " << originHit.getPhiModule() << std::endl;
	  std::cout << "originHit.getEtaModule() = " << originHit.getEtaModule() << std::endl;
	  std::cout << "originHit.getPhiIndex()  = " << originHit.getPhiIndex() << std::endl;

	  // get number of strips in module
	  std::cout << "diodes in row(0) = " << design->diodesInRow(0) << std::endl;
	  */

	  bool generate_fpgatracksimhits = true;

	  if (generate_fpgatracksimhits)
	    {
	      int n_strip = -1;
	      int etamod = originHit.getEtaModule();

	      // found earlier from diodesInRow and confirmed from ITk module specs document. 
	      if (etamod == 0 || etamod == 1 || etamod == 14 || etamod == 15)
		n_strip = 1024;
	      else if (etamod == 2 || etamod == 3 || etamod == 16 || etamod == 17)
		n_strip = 1152;
	      else if (etamod == 4 || etamod ==5)
		n_strip = 1280;
	      else if (etamod == 6 || etamod == 7)
		n_strip = 1408;
	      else if (etamod == 8 || etamod == 9)
		n_strip = 1536;
	      else if (etamod == 10 || etamod == 11 || etamod == 12 || etamod == 13)
		n_strip = 896;
	      
	      for (int phiIndex = 0; phiIndex < n_strip; phiIndex++)
		{
		  
		  Amg::Vector2D localPosition = siOriginElement->design().localPositionOfCell(InDetDD::SiCellId(phiIndex));
		  Identifier siElementPhiIndexID = siOriginElement->identifierOfPosition(localPosition);
		  const InDetDD::SiDetectorElement *siElementPhiIndex = m_SCT_mgr->getDetectorElement(siElementPhiIndexID);
		  
		  Amg::Vector2D LocalPosFPGAHit = siElementPhiIndex->rawLocalPositionOfCell(siElementPhiIndexID);
		  std::pair<Amg::Vector3D, Amg::Vector3D> endsOfStripFPGAHit = siElementPhiIndex->endsOfStrip(LocalPosFPGAHit);
		  
		  // make FPGATrackSimHit FPGAHit at phiIndex
		  FPGATrackSimHit FPGAHit;
		  FPGAHit.setHitType(HitType::unmapped);
		  FPGAHit.setDetType(SiliconTech::strip);
		  
		  // should trivially be in barrel since initial hashID.txt is strips in barrel                                
		  int barrel_ec = m_sctId->barrel_ec(siElementPhiIndexID);
		  if (barrel_ec == 0)
		    FPGAHit.setDetectorZone(DetectorZone::barrel);
		  else if (barrel_ec == 2)
		    FPGAHit.setDetectorZone(DetectorZone::posEndcap);
		  else if (barrel_ec == -2)
		    FPGAHit.setDetectorZone(DetectorZone::negEndcap);
		  
		  FPGAHit.setLayerDisk(m_sctId->layer_disk(siElementPhiIndexID));
		  FPGAHit.setPhiModule(m_sctId->phi_module(siElementPhiIndexID));
		  FPGAHit.setEtaModule(m_sctId->eta_module(siElementPhiIndexID));
		  FPGAHit.setPhiIndex(m_sctId->strip(siElementPhiIndexID));
		  FPGAHit.setEtaIndex(m_sctId->row(siElementPhiIndexID));
		  FPGAHit.setSide(m_sctId->side(siElementPhiIndexID));
		  
		  FPGAHit.setX(0.5 * (endsOfStripFPGAHit.first.x() + endsOfStripFPGAHit.second.x()));
		  FPGAHit.setY(0.5 * (endsOfStripFPGAHit.first.y() + endsOfStripFPGAHit.second.y()));
		  FPGAHit.setZ(0.5 * (endsOfStripFPGAHit.first.z() + endsOfStripFPGAHit.second.z()));

		  float R = TMath::Sqrt(FPGAHit.getX()*FPGAHit.getX() + FPGAHit.getY()*FPGAHit.getY());

		  std::cout << "event = " << count << "\tlayer = " << FPGAHit.getLayerDisk() << "\tphimod: " << FPGAHit.getPhiModule() << "\tetamod: " << FPGAHit.getEtaModule() << "\tphi index: " << FPGAHit.getPhiIndex() << "\tphiAxisX = " << siOriginElement->phiAxis().x() << "\tphiAxisY = " << siOriginElement->phiAxis().y() << "\tr: " << FPGAHit.getR() << "\tphi: " << FPGAHit.getGPhi() << "\tz: "<< FPGAHit.getZ() << std::endl;
		  
		  ofstream myfile;
		  myfile.open ("FPGATrackSimHitsStripsEndcaps_allPhiIndex_first1_reprint.dat",std::ios_base::app);
		  myfile << count << "\t" << barrel_ec << "\t" << FPGAHit.getLayerDisk() << "\t" << FPGAHit.getPhiModule() << "\t" << FPGAHit.getEtaModule() << "\t" << FPGAHit.getSide() << "\t" << FPGAHit.getPhiIndex() << "\t" << FPGAHit.getX() << "\t" << FPGAHit.getY() << "\t" << FPGAHit.getZ() << "\t" << R << "\t" << FPGAHit.getGPhi() << std::endl;
		  myfile.close();
	      
		}
	      
	  
	    }
	}
    }
  // this is for generated FPGATrackSimHits for simulated events
  auto stripSDOHandle = SG::makeHandle(m_stripSDOKey, eventContext);
  ATH_MSG_DEBUG("Found SCT SDO Map");
  auto stripRDOHandle = SG::makeHandle(m_stripRDOKey, eventContext);
  for (const InDetRawDataCollection<SCT_RDORawData>* SCT_Collection : *stripRDOHandle) {
    if (SCT_Collection == nullptr) { continue; }
    for (const SCT_RDORawData* sctRawData : *SCT_Collection) {
    
      const Identifier rdoId = sctRawData->identify();
      
      // get the det element from the det element collection
      const InDetDD::SiDetectorElement* sielement = m_SCT_mgr->getDetectorElement(rdoId);
      Amg::Vector2D LocalPos = sielement->rawLocalPositionOfCell(rdoId);
      std::pair<Amg::Vector3D, Amg::Vector3D> endsOfStrip = sielement->endsOfStrip(LocalPos);
      
      hitIndexMap[rdoId] = hitIndex;
      ++hitIndex;

      // if there is simulation truth available, try to retrieve the
      // "most likely" barcode for this strip.
      HepMC::ConstGenParticlePtr bestParent = nullptr;
      FPGATrackSimInputUtils::ParentBitmask parentMask;
      HepMcParticleLink::ExtendedBarCode bestExtcode;
      if (!m_stripSDOKey.empty()) {
        InDetSimDataCollection::const_iterator iter(stripSDOHandle->find(rdoId));
        // if SDO found for this strip, associate the particle
        if (iter != stripSDOHandle->end()) getTruthInformation(iter, parentMask, bestExtcode, bestParent);
      } // end if sct truth available
      // push back the hit information  to DataInput for HitList , copy from RawInput.cxx

      FPGATrackSimHit tmpSGhit;
      tmpSGhit.setHitType(HitType::unmapped);
      tmpSGhit.setDetType(SiliconTech::strip);
      tmpSGhit.setIdentifierHash(sielement->identifyHash());
      
      int barrel_ec = m_sctId->barrel_ec(rdoId);
      if (barrel_ec == 0)
        tmpSGhit.setDetectorZone(DetectorZone::barrel);
      else if (barrel_ec == 2)
        tmpSGhit.setDetectorZone(DetectorZone::posEndcap);
      else if (barrel_ec == -2)
        tmpSGhit.setDetectorZone(DetectorZone::negEndcap);

      tmpSGhit.setLayerDisk(m_sctId->layer_disk(rdoId));
      tmpSGhit.setPhiModule(m_sctId->phi_module(rdoId));
      tmpSGhit.setEtaModule(m_sctId->eta_module(rdoId));
      tmpSGhit.setPhiIndex(m_sctId->strip(rdoId));
      tmpSGhit.setEtaIndex(m_sctId->row(rdoId));
      tmpSGhit.setSide(m_sctId->side(rdoId));
      tmpSGhit.setEtaWidth(sctRawData->getGroupSize());
      tmpSGhit.setPhiWidth(0);
      index_type index, position;
      bestExtcode.eventIndex(index, position);
      if (bestParent)
        tmpSGhit.setEventIndex(index);
      else
        tmpSGhit.setEventIndex(std::numeric_limits<long>::max());

      tmpSGhit.setBarcode((long)(bestParent ? bestExtcode.uid() : std::numeric_limits<long>::max())); // FIXME                  
      tmpSGhit.setBarcodePt(static_cast<unsigned long>(std::ceil(bestParent ? bestParent->momentum().perp() : 0.)));
      tmpSGhit.setParentageMask(parentMask.to_ulong());
      tmpSGhit.setX(0.5 * (endsOfStrip.first.x() + endsOfStrip.second.x()));
      tmpSGhit.setY(0.5 * (endsOfStrip.first.y() + endsOfStrip.second.y()));
      tmpSGhit.setZ(0.5 * (endsOfStrip.first.z() + endsOfStrip.second.z()));
      
      /* 
      // for testing purposes
      // print out info about solidstatedetectorelementbase and FPGATrackSim hit
      std::cout << "==================================" << std::endl;
      std::cout << "rdoId = " << rdoId.getString() << std::endl;
      std::cout << "tmpSGhit layer = " << tmpSGhit.getLayerDisk() << std::endl;
      std::cout << "tmpSGhit etamod = " << tmpSGhit.getEtaModule() << std::endl;
      std::cout << "tmpSGhit phimod = " << tmpSGhit.getPhiModule() << std::endl;
      std::cout << "tmpSGhit sielement localPos[Trk::distEta] = " << LocalPos[Trk::distEta] << std::endl;
      std::cout << "tmpSGhit sielement localPos[Trk::distPhi] = " << LocalPos[Trk::distPhi] << std::endl;
      std::cout << "tmpSGhit sielement phiIndex = " << m_sctId->strip(rdoId) << std::endl;
      std::cout << "tmpSGhit sielement etaIndex = " << m_sctId->row(rdoId) << std::endl;
      std::cout << "tmpSGhit sielement phiAxis.x() = " << sielement->phiAxis().x() << std::endl;
      std::cout << "tmpSGhit sielement phiAxis.y() = " << sielement->phiAxis().y() << std::endl;
      std::cout << "tmpSGhit sielement phiAxis.z() = " << sielement->phiAxis().z() << std::endl;
      std::cout << "tmpSGhit sielement localPos[Trk::disPhi]/phiIndex = " << LocalPos[Trk::distPhi]/m_sctId->strip(rdoId) << std::endl;
      std::cout << "tmpSGhit sielement side = " << tmpSGhit.getSide() << std::endl; 
      std::cout << "tmpSGhit getX() = " << tmpSGhit.getX() << std::endl;
      std::cout << "tmpSGhit getY() = "	<< tmpSGhit.getY() << std::endl;
      std::cout << "tmpSGhit getZ() = "	<< tmpSGhit.getZ() << std::endl;
      */

      // print out FPGATrackSimHit information. May not need all of this (especially not phiAxis, print to verify it is indeed = to phiIndex 0 information). All sensors on same module (same phimod and the four adjacent etamod on the same module) have same phiAxis vector
      bool printFPGATrackSimHit = false;
      if (printFPGATrackSimHit)
	{
	  ofstream myfile;
	  myfile.open ("FPGATrackSimHits.dat",std::ios_base::app);
	  myfile << barrel_ec << "\t" << tmpSGhit.getLayerDisk() << "\t" << tmpSGhit.getPhiModule() << "\t" << tmpSGhit.getEtaModule() << "\t" << rdoId.getString() << "\t" << tmpSGhit.getSide() << "\t" << tmpSGhit.getPhiIndex() << "\t" << tmpSGhit.getX() << "\t" << tmpSGhit.getY() << "\t" << tmpSGhit.getZ() << std::endl << "\t" << sielement->phiAxis().x() << "\t" << sielement->phiAxis().y() << "\t" << sielement->phiAxis().z() << std::endl;
	  myfile.close();
	}
      
      // Add truth
      FPGATrackSimMultiTruth mt;
      FPGATrackSimMultiTruth::Barcode uniquecode(tmpSGhit.getEventIndex(), tmpSGhit.getBarcode());
      mt.maximize(uniquecode, tmpSGhit.getBarcodePt());
      tmpSGhit.setTruth(mt);

      m_eventHeader->addHit(tmpSGhit);
    } // end for each RDO in the strip collection
  } // end for each strip RDO collection
  // dump all RDO's and SDO's for a given event, for debugging purposes

  return StatusCode::SUCCESS;
}


StatusCode
FPGATrackSimSGToRawHitsTool::dumpPixelClusters(HitIndexMap& pixelClusterIndexMap, const EventContext& eventContext) {
  unsigned int pixelClusterIndex = 0;
  auto pixelSDOHandle = SG::makeHandle(m_pixelSDOKey, eventContext);
  auto pixelClusterContainerHandle = SG::makeHandle(m_pixelClusterContainerKey, eventContext);
  // Dump pixel clusters. They're in m_pixelContainer
  for (const InDet::SiClusterCollection* pixelClusterCollection : *pixelClusterContainerHandle) {
    if (pixelClusterCollection == nullptr) {
      ATH_MSG_DEBUG("pixelClusterCollection not available!");
      continue;
    }

    for (const InDet::SiCluster* cluster : *pixelClusterCollection) {
      Identifier theId = cluster->identify();
      // if there is simulation truth available, try to retrieve the "most likely" barcode for this pixel cluster.
      HepMC::ConstGenParticlePtr bestParent = nullptr;
      FPGATrackSimInputUtils::ParentBitmask parentMask;
      HepMcParticleLink::ExtendedBarCode bestExtcode;
      if (!m_pixelSDOKey.empty()) {
        for (const Identifier& rdoId : cluster->rdoList()) {
          const InDetDD::SiDetectorElement* sielement = m_PIX_mgr->getDetectorElement(rdoId);
          assert(sielement);
          InDetDD::SiCellId cellID = sielement->cellIdFromIdentifier(rdoId);

          const int nCells = sielement->numberOfConnectedCells(cellID);
          InDetSimDataCollection::const_iterator iter(pixelSDOHandle->find(rdoId));
          // this might be the ganged pixel copy.
          if (nCells > 1 && iter == pixelSDOHandle->end()) {
            InDetDD::SiReadoutCellId SiRC(m_pixelId->phi_index(rdoId), m_pixelId->eta_index(rdoId));
            for (int ii = 0; ii < nCells && iter == pixelSDOHandle->end(); ++ii) {
              iter = pixelSDOHandle->find(sielement->identifierFromCellId(sielement->design().connectedCell(SiRC, ii)));
            }
          } // end search for correct ganged pixel
          // if SDO found for this pixel, associate the particle. otherwise leave unassociated.
          if (iter != pixelSDOHandle->end()) getTruthInformation(iter, parentMask, bestExtcode, bestParent);
        } // if we have pixel sdo's available
      }
      pixelClusterIndexMap[theId] = pixelClusterIndex;
      pixelClusterIndex++;
    } // End loop over pixel clusters
  } // End loop over pixel cluster collection

  return StatusCode::SUCCESS;
}

StatusCode
FPGATrackSimSGToRawHitsTool::readOfflineClusters(std::vector <FPGATrackSimCluster>& clusters, const EventContext& eventContext)
{

  //Lets do the Pixel clusters first
  //Loopover the pixel clusters and convert them into a FPGATrackSimCluster for storage
  // Dump pixel clusters. They're in m_pixelContainer
  auto pixelSDOHandle = SG::makeHandle(m_pixelSDOKey, eventContext);
  auto pixelClusterContainerHandler = SG::makeHandle(m_pixelClusterContainerKey, eventContext);
  for (const InDet::SiClusterCollection* pixelClusterCollection : *pixelClusterContainerHandler) {
    if (pixelClusterCollection == nullptr) {
      ATH_MSG_DEBUG("pixelClusterCollection not available!");
      continue;
    }
    const int size = pixelClusterCollection->size();
    ATH_MSG_DEBUG("PixelClusterCollection found with " << size << " clusters");
    for (const InDet::SiCluster* cluster : *pixelClusterCollection) {

      // if there is simulation truth available, try to retrieve the "most likely" barcode for this pixel cluster.
      HepMC::ConstGenParticlePtr bestParent = nullptr;
      FPGATrackSimInputUtils::ParentBitmask parentMask;
      HepMcParticleLink::ExtendedBarCode bestExtcode;
      if (!m_pixelSDOKey.empty()) {
        for (const Identifier& rdoId : cluster->rdoList()) {
          const InDetDD::SiDetectorElement* sielement = m_PIX_mgr->getDetectorElement(rdoId);
          assert(sielement);
          InDetDD::SiCellId cellID = sielement->cellIdFromIdentifier(rdoId);
          const int nCells = sielement->numberOfConnectedCells(cellID);
          InDetSimDataCollection::const_iterator iter(pixelSDOHandle->find(rdoId));
          // this might be the ganged pixel copy.
          if (nCells > 1 && iter == pixelSDOHandle->end()) {
            InDetDD::SiReadoutCellId SiRC(m_pixelId->phi_index(rdoId), m_pixelId->eta_index(rdoId));
            for (int ii = 0; ii < nCells && iter == pixelSDOHandle->end(); ++ii) {
              iter = pixelSDOHandle->find(sielement->identifierFromCellId(sielement->design().connectedCell(SiRC, ii)));
            }
          } // end search for correct ganged pixel
          // if SDO found for this pixel, associate the particle. otherwise leave unassociated.
          if (iter != pixelSDOHandle->end()) getTruthInformation(iter, parentMask, bestExtcode, bestParent);
        } // if we have pixel sdo's available
      }

      Identifier theID = cluster->identify();
      //cluster object to be written out
      FPGATrackSimCluster clusterOut;
      //Rawhit object to represent the cluster
      FPGATrackSimHit clusterEquiv;
      //Lets get the information of this pixel cluster
      const InDetDD::SiDetectorElement* sielement = m_PIX_mgr->getDetectorElement(theID);
      assert(sielement);
      const InDetDD::SiLocalPosition localPos = sielement->rawLocalPositionOfCell(theID);
      const Amg::Vector3D globalPos(sielement->globalPosition(localPos));
      clusterEquiv.setHitType(HitType::clustered);
      clusterEquiv.setX(globalPos.x());
      clusterEquiv.setY(globalPos.y());
      clusterEquiv.setZ(globalPos.z());
      clusterEquiv.setDetType(SiliconTech::pixel);
      clusterEquiv.setIdentifierHash(sielement->identifyHash());

      int barrel_ec = m_pixelId->barrel_ec(theID);
      if (barrel_ec == 0)
        clusterEquiv.setDetectorZone(DetectorZone::barrel);
      else if (barrel_ec == 2)
        clusterEquiv.setDetectorZone(DetectorZone::posEndcap);
      else if (barrel_ec == -2)
        clusterEquiv.setDetectorZone(DetectorZone::negEndcap);

      clusterEquiv.setLayerDisk(m_pixelId->layer_disk(theID));
      clusterEquiv.setPhiModule(m_pixelId->phi_module(theID));
      clusterEquiv.setEtaModule(m_pixelId->eta_module(theID));
      clusterEquiv.setPhiIndex(m_pixelId->phi_index(theID));
      clusterEquiv.setEtaIndex(m_pixelId->eta_index(theID));

      clusterEquiv.setPhiWidth(cluster->width().colRow()[1]);
      clusterEquiv.setEtaWidth(cluster->width().colRow()[0]);
      //Save the truth here as the MultiTruth object is only transient
      index_type index, position;
      bestExtcode.eventIndex(index, position);
      if (bestParent)
        clusterEquiv.setEventIndex(index);
      else
        clusterEquiv.setEventIndex(std::numeric_limits<long>::max());
      clusterEquiv.setBarcode((long)(bestParent ? bestExtcode.uid() : std::numeric_limits<long>::max())); // FIXME
      clusterEquiv.setBarcodePt(static_cast<unsigned long>(std::ceil(bestParent ? bestParent->momentum().perp() : 0.)));
      clusterEquiv.setParentageMask(parentMask.to_ulong());
      clusterOut.setClusterEquiv(clusterEquiv);
      clusters.push_back(clusterOut);
    }
  }

  //Now lets do the strip clusters
  //Loopover the pixel clusters and convert them into a FPGATrackSimCluster for storage
  // Dump pixel clusters. They're in m_pixelContainer
  auto stripSDOHandle = SG::makeHandle(m_stripSDOKey, eventContext);
  ATH_MSG_DEBUG("Found SCT SDO Map");
  auto stripRDOHandle = SG::makeHandle(m_stripRDOKey, eventContext);

  for (const InDetRawDataCollection<SCT_RDORawData>* SCT_Collection : *stripRDOHandle) {
    if (SCT_Collection == nullptr) { continue; }
    for (const SCT_RDORawData* sctRawData : *SCT_Collection) {
      const Identifier rdoId = sctRawData->identify();
      // get the det element from the det element collection
      const InDetDD::SiDetectorElement* sielement = m_SCT_mgr->getDetectorElement(rdoId);
      const InDetDD::SiDetectorDesign& design = dynamic_cast<const InDetDD::SiDetectorDesign&>(sielement->design());
      const InDetDD::SiLocalPosition localPos = design.localPositionOfCell(m_sctId->strip(rdoId));
      const Amg::Vector3D gPos = sielement->globalPosition(localPos);
      // if there is simulation truth available, try to retrieve the
      // "most likely" barcode for this strip.
      HepMC::ConstGenParticlePtr bestParent = nullptr;
      FPGATrackSimInputUtils::ParentBitmask parentMask;
      HepMcParticleLink::ExtendedBarCode bestExtcode;
      if (!m_stripSDOKey.empty()) {
        InDetSimDataCollection::const_iterator iter(stripSDOHandle->find(rdoId));
        // if SDO found for this pixel, associate the particle
        if (iter != stripSDOHandle->end()) getTruthInformation(iter, parentMask, bestExtcode, bestParent);
      } // end if sct truth available

      // push back the hit information  to DataInput for HitList , copy from RawInput.cxx
      FPGATrackSimCluster clusterOut;
      FPGATrackSimHit clusterEquiv;
      clusterEquiv.setHitType(HitType::clustered);
      clusterEquiv.setX(gPos.x());
      clusterEquiv.setY(gPos.y());
      clusterEquiv.setZ(gPos.z());
      clusterEquiv.setDetType(SiliconTech::strip);
      clusterEquiv.setIdentifierHash(sielement->identifyHash());

      int barrel_ec = m_sctId->barrel_ec(rdoId);
      if (barrel_ec == 0)
        clusterEquiv.setDetectorZone(DetectorZone::barrel);
      else if (barrel_ec == 2)
        clusterEquiv.setDetectorZone(DetectorZone::posEndcap);
      else if (barrel_ec == -2)
        clusterEquiv.setDetectorZone(DetectorZone::negEndcap);

      clusterEquiv.setLayerDisk(m_sctId->layer_disk(rdoId));
      clusterEquiv.setPhiModule(m_sctId->phi_module(rdoId));
      clusterEquiv.setEtaModule(m_sctId->eta_module(rdoId));
      clusterEquiv.setPhiIndex(m_sctId->strip(rdoId));
      clusterEquiv.setEtaIndex(m_sctId->row(rdoId));
      clusterEquiv.setSide(m_sctId->side(rdoId));
      //I think this is the strip "cluster" width
      clusterEquiv.setPhiWidth(sctRawData->getGroupSize());
      //Save the truth here as the MultiTruth object is only transient
      index_type index, position;
      bestExtcode.eventIndex(index, position);
      if (bestParent)
        clusterEquiv.setEventIndex(index);
      else
        clusterEquiv.setEventIndex(std::numeric_limits<long>::max());


      clusterEquiv.setBarcode((long)(bestParent ? bestExtcode.uid() : std::numeric_limits<long>::max())); // FIXME
      clusterEquiv.setBarcodePt(static_cast<unsigned long>(std::ceil(bestParent ? bestParent->momentum().perp() : 0.)));
      clusterEquiv.setParentageMask(parentMask.to_ulong());
      clusterOut.setClusterEquiv(clusterEquiv);
      clusters.push_back(clusterOut);
    } // end for each RDO in the strip collection
  } // end for each strip RDO collection
  // dump all RDO's and SDO's for a given event, for debugging purposes

  return StatusCode::SUCCESS;
}

StatusCode
FPGATrackSimSGToRawHitsTool::readTruthTracks(std::vector <FPGATrackSimTruthTrack>& truth, const EventContext& eventContext)
{
  auto simTracksHandle = SG::makeHandle(m_mcCollectionKey, eventContext);
  ATH_MSG_DEBUG("Dump truth tracks, size " << simTracksHandle->size());

  // dump each truth track
  for (unsigned int ievt = 0; ievt < simTracksHandle->size(); ++ievt) {
    const HepMC::GenEvent* genEvent = simTracksHandle->at(ievt);
    // retrieve the primary interaction vertex here. for now, use the dummy origin.
    HepGeom::Point3D<double>  primaryVtx(0., 0., 0.);
    // the event should have signal process vertex unless it was generated as single particles.
    // if it exists, use it for the primary vertex.
    HepMC::ConstGenVertexPtr spv = HepMC::signal_process_vertex(genEvent);
    if (spv) {
        primaryVtx.set(spv->position().x(),
                       spv->position().y(),
                       spv->position().z());
      ATH_MSG_DEBUG("using signal process vertex for eventIndex " << ievt << ":"
        << primaryVtx.x() << "\t" << primaryVtx.y() << "\t" << primaryVtx.z());
    }
    for (const auto& particle: *genEvent) {
      const int pdgcode = particle->pdg_id();
      // reject generated particles without a production vertex.
      if (particle->production_vertex() == nullptr) {
        continue;
      }
      // reject neutral or unstable particles
      const HepPDT::ParticleData* pd = m_particleDataTable->particle(abs(pdgcode));
      if (pd == nullptr) {
        continue;
      }
      float charge = pd->charge();
      if (pdgcode < 0) charge *= -1.; // since we took absolute value above
      if (std::abs(charge) < 0.5) {
        continue;
      }
      if (!MC::isStable(particle)) {
        continue;
      }
      // truth-to-track tool
      const Amg::Vector3D momentum(particle->momentum().px(), particle->momentum().py(), particle->momentum().pz());
      const Amg::Vector3D position(particle->production_vertex()->position().x(), particle->production_vertex()->position().y(), particle->production_vertex()->position().z());
      const Trk::CurvilinearParameters cParameters(position, momentum, charge);
      Trk::PerigeeSurface persf;
      if (m_UseNominalOrigin) {
        Amg::Vector3D    origin(0, 0, 0);
        persf = Trk::PerigeeSurface(origin);
      }
      else {
        SG::ReadCondHandle<InDet::BeamSpotData> beamSpotHandle{ m_beamSpotKey, eventContext };
        Trk::PerigeeSurface persf(beamSpotHandle->beamPos());
      }
      const Trk::TrackParameters* tP = m_extrapolator->extrapolate(eventContext, cParameters, persf, Trk::anyDirection, false).release();
      const double track_truth_d0 = tP ? tP->parameters()[Trk::d0] : 999.;
      const double track_truth_phi = tP ? tP->parameters()[Trk::phi] : 999.;
      const double track_truth_p = (tP && fabs(tP->parameters()[Trk::qOverP]) > 1.e-8) ?
        tP->charge() / tP->parameters()[Trk::qOverP] : 10E7;
      const double track_truth_x0 = tP ? tP->position().x() : 999.;
      const double track_truth_y0 = tP ? tP->position().y() : 999.;
      const double track_truth_z0 = tP ? tP->parameters()[Trk::z0] : 999.;
      const double track_truth_q = tP ? tP->charge() : 0.;
      const double track_truth_sinphi = tP ? std::sin(tP->parameters()[Trk::phi]) : -1.;
      const double track_truth_cosphi = tP ? std::cos(tP->parameters()[Trk::phi]) : -1.;
      const double track_truth_sintheta = tP ? std::sin(tP->parameters()[Trk::theta]) : -1.;
      const double track_truth_costheta = tP ? std::cos(tP->parameters()[Trk::theta]) : -1.;
      double truth_d0corr = track_truth_d0 - (primaryVtx.y() * cos(track_truth_phi) - primaryVtx.x() * sin(track_truth_phi));
      double truth_zvertex = 0.;
      const HepGeom::Point3D<double> startVertex(particle->production_vertex()->position().x(), particle->production_vertex()->position().y(), particle->production_vertex()->position().z());
      // categorize particle (prompt, secondary, etc.) based on InDetPerformanceRTT/detector paper criteria.
      bool isPrimary = true;
      if (std::abs(truth_d0corr) > 2.) { isPrimary = false; }
      const int bc = HepMC::barcode(particle); // FIXME update barcode-based syntax
      if (HepMC::is_simulation_particle(particle) || bc == 0) { isPrimary = false; } // FIXME update barcode-based syntax
      if (isPrimary && particle->production_vertex()) {
        const HepGeom::Point3D<double> startVertex(particle->production_vertex()->position().x(), particle->production_vertex()->position().y(), particle->production_vertex()->position().z());
        if (std::abs(startVertex.z() - truth_zvertex) > 100.) { isPrimary = false; }
        if (particle->end_vertex()) {
          HepGeom::Point3D<double> endVertex(particle->end_vertex()->position().x(), particle->end_vertex()->position().y(), particle->end_vertex()->position().z());
          if (endVertex.perp() < FPGATrackSim_PT_TRUTHMIN && std::abs(endVertex.z()) < FPGATrackSim_Z_TRUTHMIN) { isPrimary = false; }
        }
      }
      else {
        isPrimary = false;
      }

      HepMcParticleLink::ExtendedBarCode extBarcode2(bc, ievt,
                                                     HepMcParticleLink::IS_EVENTNUM,
                                                     HepMcParticleLink::IS_BARCODE); // FIXME update barcode-based syntax

      FPGATrackSimTruthTrack tmpSGTrack;
      tmpSGTrack.setVtxX(track_truth_x0);
      tmpSGTrack.setVtxY(track_truth_y0);
      tmpSGTrack.setVtxZ(track_truth_z0);
      tmpSGTrack.setD0(track_truth_d0);
      tmpSGTrack.setZ0(track_truth_z0);
      tmpSGTrack.setVtxZ(primaryVtx.z());
      tmpSGTrack.setQ(track_truth_q);
      tmpSGTrack.setPX(track_truth_p * (track_truth_cosphi * track_truth_sintheta));
      tmpSGTrack.setPY(track_truth_p * (track_truth_sinphi * track_truth_sintheta));
      tmpSGTrack.setPZ(track_truth_p * track_truth_costheta);
      tmpSGTrack.setPDGCode(pdgcode);
      tmpSGTrack.setStatus(particle->status());
      tmpSGTrack.setBarcode(extBarcode2.uid()); // FIXME
      index_type index2, position2;
      extBarcode2.eventIndex(index2, position2);
      tmpSGTrack.setEventIndex(index2);

      truth.push_back(tmpSGTrack);
    } // end for each GenParticle in this GenEvent
  } // end for each GenEvent


  return StatusCode::SUCCESS;
}

void FPGATrackSimSGToRawHitsTool::getTruthInformation(InDetSimDataCollection::const_iterator& iter,
  FPGATrackSimInputUtils::ParentBitmask& parentMask,
  HepMcParticleLink::ExtendedBarCode& bestExtcode,
  HepMC::ConstGenParticlePtr& bestParent) {

  const InDetSimData& sdo(iter->second);
  const std::vector<InDetSimData::Deposit>& deposits(sdo.getdeposits());
  for (const InDetSimData::Deposit& dep : deposits) {

    const HepMcParticleLink& particleLink = dep.first;
    // RDO's without SDO's are delta rays or detector noise.
    if (!particleLink.isValid()) { continue; }
    const float genEta = particleLink->momentum().pseudoRapidity();
    const float genPt = particleLink->momentum().perp(); // MeV
    // reject unstable particles
    if (!MC::isStable(particleLink)) { continue; }
    // reject secondaries and low pT (<400 MeV) pileup
    if (HepMC::is_simulation_particle(particleLink.cptr()) ||particleLink.barcode() == 0) { continue; }  // FIXME
    // reject far forward particles
    if (std::fabs(genEta) > m_maxEta) { continue; }
    // "bestParent" is the highest pt particle
    if (bestParent == nullptr || bestParent->momentum().perp() < genPt) {
      bestParent = particleLink.cptr();
      bestExtcode = HepMcParticleLink::ExtendedBarCode(particleLink.barcode(), particleLink.eventIndex(), HepMcParticleLink::IS_EVENTNUM, HepMcParticleLink::IS_BARCODE); // FIXME barcode-based-syntax
    }
 #ifdef HEPMC3
     parentMask |= FPGATrackSimInputUtils::construct_truth_bitmap(std::shared_ptr<const HepMC3::GenParticle>(particleLink.cptr()));
 #else
     parentMask |= FPGATrackSimInputUtils::construct_truth_bitmap(particleLink.cptr());
 #endif
     // check SDO
  } // end for each contributing particle

}
